﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace assignment_3.Models.Domain
{
    [Table("Character")]
    public class Character
    {
        public int Id { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(120)]
        [Required]
        public string FullName { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(120)]
        [Required]
        public string? Alias { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string Gender { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(500)]
        public string? PictureURL { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}
