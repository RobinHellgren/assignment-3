﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace assignment_3.Models.Domain
{
    public class Franchise
    {
        public int Id { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(120)]
        [Required]
        public string Name { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(1200)]
        [Required]
        public string Description { get; set; }
        public ICollection<Movie> Movies { get; set; }
    }
}
