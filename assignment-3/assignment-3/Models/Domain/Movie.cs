﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace assignment_3.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        public int Id { get; set; }
        [Column(TypeName="nvarchar")]
        [MaxLength(120)]
        [Required]
        public string Title { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(90)]
        [Required]
        public string Genre { get; set; }
        [Column(TypeName = "int")]
        [Required]
        public int ReleaseYear { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(120)]
        [Required]
        public string Director { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(120)]
        public string Picture { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(500)]
        public string Trailer { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }

    }
}
