﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3.Models.DTO.CharacterDTO
{
    /// <summary>
    /// The DTO with relevant fields when adding a character.
    /// </summary>
    public class CharacterAddDTO
    {
        /// <summary>
        /// The Full Name of the character.
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// The Alias of the character.
        /// </summary>
        public string? Alias { get; set; }
        /// <summary>
        /// The gender of the character.
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// A URL to a picture of the character.
        /// </summary>
        public string? PictureURL { get; set; }
    }
}
