﻿using assignment_3.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3.Models.DTO.CharacterDTO
{
    /// <summary>
    /// The DTO with relevant fields when showing a character and its movies.
    /// </summary>
    public class CharacterReadDTOWithMovies
    {
        /// <summary>
        /// The ID of the character.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The fullname of the character.
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// The alias of the character.
        /// </summary>
        public string? Alias { get; set; }
        /// <summary>
        /// The gender of the character.
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// A URL to a picture of the character.
        /// </summary>
        public string? PictureURL { get; set; }
        /// <summary>
        /// The movies that the character appears in.
        /// </summary>
        public ICollection<string> Movies { get; set; }

    }
}
