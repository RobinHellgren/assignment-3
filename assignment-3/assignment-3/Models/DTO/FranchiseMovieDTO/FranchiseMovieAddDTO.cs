﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3.Models.DTO.FranchiseMovieDTO
{
    /// <summary>
    /// The DTO containing the relevant fields when adding a franchise to a movie.
    /// </summary>
    public class FranchiseMovieAddDTO
    {
        /// <summary>
        /// The ID of the franchise.
        /// </summary>
        public int FranchiseId { get; set; }
        /// <summary>
        /// The ID of the movie.
        /// </summary>
        public int MovieId { get; set; }
    }
}
