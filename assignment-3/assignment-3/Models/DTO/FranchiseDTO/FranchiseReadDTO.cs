﻿using assignment_3.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3.Models.DTO.FranchiseDTO
{
    /// <summary>
    /// The DTO with relevant fields when reading a franchise.
    /// </summary>
    public class FranchiseReadDTO
    {
        /// <summary>
        /// The ID of the franchise.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The name of the franchise.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The description of the franchise.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The titles of the movies associated with the franchise.
        /// </summary>
        public ICollection<string> Movies { get; set; }
    }
}
