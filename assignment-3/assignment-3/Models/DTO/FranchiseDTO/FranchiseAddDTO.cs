﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace assignment_3.Models.DTO.FranchiseDTO
{
    /// <summary>
    /// The DTO with relevant fields when adding a franchise.
    /// </summary>
    public class FranchiseAddDTO
    {
        /// <summary>
        /// The name of the franchise
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The description of the franchise
        /// </summary>
        public string Description { get; set; }

    }
}
