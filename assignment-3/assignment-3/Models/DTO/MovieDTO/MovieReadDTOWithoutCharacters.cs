﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3.Models.DTO.MovieDTO
{
    /// <summary>
    /// The DTO with relevant fields when adding a movie without the characters.
    /// </summary>
    public class MovieReadDTOWithoutCharacters
    {
        public int Id { get; set; }
        /// <summary>
        /// The title of the movie.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// The movies genre.
        /// </summary>
        public string Genre { get; set; }
        /// <summary>
        /// The movies release year.
        /// </summary>
        public int ReleaseYear { get; set; }
        /// <summary>
        /// The movies director.
        /// </summary>
        public string Director { get; set; }
        /// <summary>
        /// A URL to a picture from the movie.
        /// </summary>
        public string Picture { get; set; }
        /// <summary>
        /// A URL to a trailer for the movie.
        /// </summary>
        public string Trailer { get; set; }
        /// <summary>
        /// The ID of the movies franchise.
        /// </summary>
        public int? FranchiseId { get; set; }
    }

}
