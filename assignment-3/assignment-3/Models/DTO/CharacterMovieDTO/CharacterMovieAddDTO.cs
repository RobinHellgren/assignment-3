﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3.Models.DTO.CharacterMovieDTO
{
    /// <summary>
    /// The DTO with relevant fields when adding a movie to a character.
    /// </summary>
    public class CharacterMovieAddDTO
    {
        /// <summary>
        /// The ID of the character.
        /// </summary>
        public int CharacterId { get; set; }
        /// <summary>
        /// The ID of the movie.
        /// </summary>
        public int MovieId { get; set; }
    }
}
