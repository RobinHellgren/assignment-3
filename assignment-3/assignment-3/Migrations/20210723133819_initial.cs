﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace assignment_3.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    PictureURL = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(1200)", maxLength: 1200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(90)", maxLength: 90, nullable: false),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: false),
                    Picture = table.Column<string>(type: "nvarchar(120)", maxLength: 120, nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movie_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacter",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false),
                    MovieId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacter", x => new { x.CharacterId, x.MovieId });
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "PictureURL" },
                values: new object[,]
                {
                    { -1, "Legolas", "Orlando Bloom", "M", "https://en.wikipedia.org/wiki/File:Orlando_Bloom_Cannes_2013.jpg" },
                    { -2, "Frodo", "Elijha Wood", "M", "https://en.wikipedia.org/wiki/File:Orlando_Bloom_Cannes_2013.jpg" },
                    { -3, "Han Solo", "Harrison Ford", "M", "https://en.wikipedia.org/wiki/File:Orlando_Bloom_Cannes_2013.jpg" },
                    { -4, "Princess Leia", "Carrie Fisher", "F", "https://en.wikipedia.org/wiki/File:Orlando_Bloom_Cannes_2013.jpg" },
                    { -5, "Galadriel", "Cate Blanchett", "F", "https://en.wikipedia.org/wiki/File:Orlando_Bloom_Cannes_2013.jpg" },
                    { -6, "Chewbacca \"Chewie\" Attichitcuk", "Peter Mayhew", "M", "https://en.wikipedia.org/wiki/File:Orlando_Bloom_Cannes_2013.jpg" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { -1, "De ska ha ringen , den ska till berget och slängas - pantas. Sauron ARG.", "Sagan om Ringen" },
                    { -2, "Laser go BOOOOOOOOOOOOOOOOOM Alderaan go NOOOOOOOOOOOOO", "Star Wars" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { -1, "Peter Jackson", -1, "Fantasy", null, 2001, "Härskarringen", "https://www.youtube.com/watch?v=UEXXdpEsmBk" });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { -2, "Peter Jackson", -1, "Fantasy", null, 2004, "Sagan om de Två Tornen", "https://www.youtube.com/watch?v=UEXXdpEsmBk" });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[] { -3, "George Lucas", -2, "Episk Rymdopera Film", null, 1977, "Star Wars: A New Hope", "https://www.youtube.com/watch?v=UEXXdpEsmBk" });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[] { -1, -1 });

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movie",
                column: "FranchiseId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_MovieId",
                table: "MovieCharacter",
                column: "MovieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacter");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
