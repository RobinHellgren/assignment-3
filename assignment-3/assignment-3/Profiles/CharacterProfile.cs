﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using assignment_3.Models.Domain;
using assignment_3.Models.DTO.CharacterDTO;

namespace assignment_3.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTOWithMovies>()
                .ForMember(cdto => cdto.Movies,
                    opt => opt.MapFrom(c => c.Movies
                        .Select(m => m.Title).ToArray()));
            
            CreateMap<Character, CharacterReadDTOMovieId>()
                .ForMember(cdto => cdto.Movies,
                    opt => opt.MapFrom(c => c.Movies
                        .Select(m => m.Id).ToArray()));

            CreateMap<CharacterReadDTOMovieId, Character>()
                .ForMember(cdto => cdto.Movies,
                    opt => opt.MapFrom(c => c.Movies
                        .Select(id => new Movie() { Id = id })));

            CreateMap<Character, CharacterAddDTO>().ReverseMap();

            CreateMap<Character, CharacterUpdateDTO>().ReverseMap();

            CreateMap<Character, CharacterReadDTOWithoutMovies>();

        }
    }
}
