﻿using assignment_3.Models.Domain;
using assignment_3.Models.DTO.MovieDTO;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_3.Profiles
{
    public class MovieProfile : Profile
    {


        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTOWithoutCharacters>();

            CreateMap<Movie, MovieReadDTO>()
              .ForMember(mdto => mdto.Characters,
                  opt => opt.MapFrom(c => c.Characters
                      .Select(c => c.FullName).ToArray()));

            CreateMap<Movie, MovieAddDTO>().ReverseMap();

            CreateMap<Movie, MovieUpdateDTO>().ReverseMap();

        }
    }
}
