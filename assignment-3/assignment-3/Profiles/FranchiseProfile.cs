﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using assignment_3.Models.Domain;
using assignment_3.Models.DTO.FranchiseDTO;

namespace assignment_3.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(mdto => mdto.Movies,
                    opt => opt.MapFrom(c => c.Movies
                        .Select(c => c.Title).ToArray()));

            CreateMap<Franchise, FranchiseAddDTO>().ReverseMap();

            CreateMap<Franchise, FranchiseUpdateDTO>().ReverseMap();

        }
    }
}
