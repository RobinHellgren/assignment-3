﻿using assignment_3.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using assignment_3.Models.DTO.CharacterDTO;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using assignment_3.Models.Domain;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using assignment_3.Models.DTO.CharacterMovieDTO;
using Microsoft.AspNetCore.Http;

namespace assignment_3.Controllers
{
    [Route("api/v1/Characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly MovieDatabaseContext _context;

        private readonly IMapper _mapper;

        public CharactersController(MovieDatabaseContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        
        /// <summary>
        /// Returns a list of all characters in the database
        /// </summary>
        /// <returns>A list of all characters in the database</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTOWithMovies>>> GetCharacters()
        {
            var characterList = await _context.Characters.Include(m => m.Movies).ToListAsync();
            
            if(characterList.Count == 0)
            {
                return NotFound();
            }
            List<CharacterReadDTOWithMovies> dtoList = _mapper.Map<List<CharacterReadDTOWithMovies>>(characterList);

            return Ok(dtoList);
        }
        
        /// <summary>
        /// Get character with the specified id
        /// </summary>
        /// <param name="characterId">Id of the character</param>
        /// <returns>CharacterReadDTOWithMovies object</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("{characterId}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTOWithMovies>>> GetCharacterById([FromRoute] int characterId)
        {
            var characterList = await _context.Characters.Include(m => m.Movies).Where(c => c.Id ==characterId).ToListAsync();
            
            if (characterList.Count == 0)
            {
                return NotFound();
            }

            List<CharacterReadDTOWithMovies> dtoList = _mapper.Map<List<CharacterReadDTOWithMovies>>(characterList);

            return Ok(dtoList);
        }


        /// <summary>
        /// Adds a character to the database.
        /// </summary>
        /// <param name="addDTO">Contains the data for the new character.</param>
        /// <returns>Ok</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTOWithoutMovies>> AddCharacter([FromBody] CharacterAddDTO addDTO)
        {
            Character character = _mapper.Map<Character>(addDTO);
            EntityEntry newEntry = _context.Characters.Add(character);
            
            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            CharacterReadDTOWithoutMovies newCharacter = _mapper.Map<CharacterReadDTOWithoutMovies>(newEntry.Entity);

            return CreatedAtAction("AddCharacter", new { id = newCharacter.Id }, newCharacter);
        }
        
        /// <summary>
        /// Updates the specified character in the database
        /// </summary>
        /// <param name="characterId">Id of the character that is being updated</param>
        /// <param name="updatedCharacter">DTO containing the new values for the character</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{characterId}")]
        public async Task<ActionResult> PutCharacter(int characterId, CharacterUpdateDTO updatedCharacter)
        {
            if (characterId != updatedCharacter.Id)
            {
                return BadRequest();
            }

            Character domainCharacter = _mapper.Map<Character>(updatedCharacter);
            _context.Entry(domainCharacter).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }
            return NoContent();
        }

        /// <summary>
        /// Removes the specified character from the database.
        /// </summary>
        /// <param name="characterId">Id of the character that is removed from the database</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{characterId}")]
        public async Task<ActionResult> DeleteCharacter([FromRoute] int characterId)
        {
            var character = await _context.Characters.FindAsync(characterId);
            if(character == null)
            {
                return NotFound();
            }
            _context.Characters.Remove(character);
            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            return NoContent();
        }
        
        /// <summary>
        /// Adds a character to a movie, updating the many to many relationship in the database
        /// </summary>
        /// <param name="characterId">Id of the character that is added</param>
        /// <param name="movieId">Id of the movie the character is added to</param>
        /// <param name="characterMovieIds">DTO object containing the IDs of the movie and character</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPut("/AddCharacterToMovie/{characterId}/{movieId}")]
        public async Task<ActionResult> AddCharacterToMovie([FromRoute] int characterId, [FromRoute] int movieId, [FromBody] CharacterMovieAddDTO characterMovieIds)
        {
            if (characterId != characterMovieIds.CharacterId || movieId != characterMovieIds.MovieId)
            {
                return BadRequest();
            }
            Character character = _context.Characters.Include("Movies").FirstOrDefault(c => c.Id == characterMovieIds.CharacterId);
            character.Movies.Add(_context.Movies.Find(characterMovieIds.MovieId));
            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            return CreatedAtAction("AddCharacterToMovie", characterMovieIds);
        }

        /// <summary>
        /// Gets a list of characters from the given movie
        /// </summary>
        /// <param name="movieId">Id of the movie to fetch characters from</param>
        /// <returns>List of CharacterReadDTOWithoutMovies objects</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("/GetsCharactersFromAMovie/{movieId}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTOWithoutMovies>>> GetCharactersInMovie([FromRoute] int movieId)
        {

            var movie = await _context.Movies.Include(c => c.Characters).FirstOrDefaultAsync(i => i.Id == movieId);
            
            if(movie == null)
            {
                return NotFound();
            }

            var characterList = movie.Characters.ToList();

            List<CharacterReadDTOWithoutMovies> dtoList = _mapper.Map<List<CharacterReadDTOWithoutMovies>>(characterList);

            return Ok(dtoList);
        }

        /// <summary>
        /// Removes a character from a movie, updating the many to many relationship in the database
        /// </summary>
        /// <param name="characterId">Id of the character that is removed</param>
        /// <param name="movieId">Id of the movie the character is removed from</param>
        /// <param name="characterMovieIds">DTO object containing the IDs of the movie and character</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpDelete("/RemoveCharacterFromMovie/{characterId}/{movieId}")]
        public async Task<ActionResult> RemoveCharacterFromMovie([FromRoute] int characterId, [FromRoute] int movieId, [FromBody] CharacterMovieAddDTO characterMovieIds)
        {
            if (characterId != characterMovieIds.CharacterId || movieId != characterMovieIds.MovieId)
            {
                return BadRequest();
            }
            Character character = _context.Characters.Include("Movies").FirstOrDefault(c => c.Id == characterMovieIds.CharacterId);
            if (character == null)
            {
                return NotFound();
            }
            character.Movies.Remove(_context.Movies.Find(characterMovieIds.MovieId));
            
            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            return NoContent();
        }

    }
}
