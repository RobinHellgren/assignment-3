using assignment_3.Models;
using assignment_3.Models.Domain;
using assignment_3.Models.DTO.MovieDTO;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace assignment_3.Controllers
{

    [Route("api/v1/Movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]    
    public class MoviesController : ControllerBase
    {
        private readonly MovieDatabaseContext _context;

        private readonly IMapper _mapper;

        public MoviesController(MovieDatabaseContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        /// <summary>
        /// Gets a specific movie by id,
        /// </summary>
        /// <param name="movieId">Id of the book to get</param>
        /// <returns>Returns a MovieReadDTO list.</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("{Id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovieById([FromRoute] int movieId)
        {
            var movie = await _context.Movies.Include(c => c.Characters).FirstOrDefaultAsync(i => i.Id == movieId); ;

            if (movie == null)
            {
                return NotFound();
            }

            MovieReadDTO dtoMovie = _mapper.Map<MovieReadDTO>(movie);

            return Ok(dtoMovie);
        }


        /// <summary>
        /// Gets all movies.
        /// </summary>
        /// <returns> A MovieReadDTO list.</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movieList = await _context.Movies.Include(c => c.Characters).ToListAsync();
            if (movieList.Count == 0)
            {
                return NotFound();
            }
            List<MovieReadDTO> dtoList = _mapper.Map<List<MovieReadDTO>>(movieList);

            return Ok(dtoList);
        }


        /// <summary>
        /// Adds a new Movie.
        /// </summary>
        /// <param name="movie">A MovieAddDTO containing the new values. </param>
        /// <returns>The created MovieReadDTO. </returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie([FromBody] MovieAddDTO movie)
        {
            Movie domainMovie = _mapper.Map<Movie>(movie);

            try
            {
                _context.Movies.Add(domainMovie);
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            MovieReadDTO newMovie = _mapper.Map<MovieReadDTO>(domainMovie);

            return CreatedAtAction("GetMovieById", new { id = newMovie.Id }, newMovie);

        }



        /// <summary>
        /// Updates a movie.
        /// </summary>
        /// <param name="movieId">The id of the movie to update.</param>
        /// <param name="updatedMovie">The movie with updated attributes.</param>
        /// <returns>NoContent</returns>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut("{movieId}")]
        public async Task<ActionResult> PutMovie(int movieId, MovieUpdateDTO updatedMovie)
        {
            if (movieId != updatedMovie.Id)
            {
                return BadRequest();
            }

            Movie domainMovie = _mapper.Map<Movie>(updatedMovie);
            _context.Entry(domainMovie).State = EntityState.Modified;


            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }


            return NoContent();
        }


        /// <summary>
        /// Deltes a movie by id.
        /// </summary>
        /// <param name="movieId">The id of the movie to remove.</param>
        /// <returns>NoContent</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{movieId}")]
        public async Task<ActionResult> DeleteMovie(int movieId)
        {
            var domainmovie = await _context.Movies.FindAsync(movieId);

            if (domainmovie == null)
            {
                return NotFound();
            }

            try
            {
                _context.Movies.Remove(domainmovie);

                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            return NoContent();
        }
        /// <summary>
        /// Gets the all the movies of a franchise.
        /// </summary>
        /// <param name="franchiseId">The given ID of the franchise that will have its movies presented.</param>
        /// <returns>A MoviesReadDTO list.</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("/GetMoviesInFranchise/{franchiseId}")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise([FromRoute] int franchiseId)
        {
            var movieList = await _context.Movies.Where(m => m.FranchiseId == franchiseId).ToListAsync();
            if (movieList.Count == 0)
            {
                return NotFound();
            }
            List<MovieReadDTO> dtoList = _mapper.Map<List<MovieReadDTO>>(movieList);

            return Ok(dtoList);
        }
    }
}
