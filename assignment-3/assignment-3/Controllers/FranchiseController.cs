﻿using assignment_3.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using assignment_3.Models.Domain;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using assignment_3.Models.DTO.FranchiseDTO;
using assignment_3.Models.DTO.CharacterDTO;
using assignment_3.Models.DTO.FranchiseMovieDTO;
using Microsoft.AspNetCore.Http;

namespace assignment_3.Controllers
{
    [Route("api/v1/Franchise")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly MovieDatabaseContext _context;

        private readonly IMapper _mapper;

        public FranchiseController(MovieDatabaseContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all franchises from the database.
        /// </summary>
        /// <returns>A FranchiseReadDTO list</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchiseList = await _context.Franchises.Include(m => m.Movies).ToListAsync();
            if(franchiseList.Count == 0)
            {
                return NotFound();
            }
            List<FranchiseReadDTO> dtoList = _mapper.Map<List<FranchiseReadDTO>>(franchiseList);

            return Ok(dtoList);
        }
        /// <summary>
        /// Takes an ID and returns that corresponding franchise.
        /// </summary>
        /// <param name="franchiseId"> The given franchise ID</param>
        /// <returns>A FranchiseReadDTO list</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("{franchiseId}")]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchiseById([FromRoute] int franchiseId)
        {
            var franchise = await _context.Franchises.Include(m => m.Movies).Where(c => c.Id == franchiseId).ToListAsync();
            if (franchise.Count == 0)
            {
                return NotFound();
            }
            FranchiseReadDTO dto = _mapper.Map<FranchiseReadDTO>(franchise);

            return Ok(dto);
        }
        /// <summary>
        /// Creates a new franchise with the given values.
        /// </summary>
        /// <param name="addDTO">A FranchiseAddDTO containing the values of the new franchise.</param>
        /// <returns>A FranchiseReadDTO list</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> AddFranchise([FromBody] FranchiseAddDTO addDTO)
        {
            Franchise franchise = _mapper.Map<Franchise>(addDTO);
            EntityEntry newEntry = _context.Franchises.Add(franchise);
            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }
            FranchiseReadDTO newCharacter = _mapper.Map<FranchiseReadDTO>(newEntry.Entity);

            return CreatedAtAction("AddFranchise", newCharacter);
        }
        /// <summary>
        /// Updates the franchise with the given ID.
        /// </summary>
        /// <param name="franchiseId">The ID of the franchise that is updated</param>
        /// <param name="updatedFranchise">The new values that is updated.</param>
        /// <returns> NoContent http response</returns>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{franchiseId}")]
        public async Task<ActionResult> PutFranchise(int franchiseId, FranchiseUpdateDTO updatedFranchise)
        {
            if (franchiseId != updatedFranchise.Id)
            {
                return BadRequest();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(updatedFranchise);
            _context.Entry(domainFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            return NoContent();
        }
        /// <summary>
        /// Deletes the franchise with the given ID.
        /// </summary>
        /// <param name="franchiseId">The given ID of the franchise that will be deleted.</param>
        /// <returns>NoContent</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{franchiseId}")]
        public async Task<ActionResult> DeleteFranchise([FromRoute] int franchiseId)
        {
            var franchise = await _context.Franchises.FindAsync(franchiseId);
            if (franchise == null)
            {
                return NotFound();
            }
            _context.Franchises.Remove(franchise);
            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            return NoContent();
        }
        /// <summary>
        /// Gets a list of all characters from the given franchise
        /// </summary>
        /// <param name="franchiseId">The id of the franchise that is queried</param>
        /// <returns>A list of CharacterReadDTOWithoutMovies objects</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("/GetCharactersFromAFranchise/{franchiseId}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTOWithoutMovies>>> GetCharactersFromFranchise([FromRoute] int franchiseId)
        {
            List<Movie> movies = await _context.Movies.Include(m => m.Characters).Where(m => m.FranchiseId == franchiseId).ToListAsync();
            if(movies.Count == 0)
            {
                return NotFound();
            }
            List<Character> characterList = new List<Character>();
            foreach (var movie in movies)
            {
                characterList.AddRange(movie.Characters);
            }
            characterList = characterList.Distinct().ToList();
            List<CharacterReadDTOWithoutMovies> dtoList = _mapper.Map<List<CharacterReadDTOWithoutMovies>>(characterList);
            return Ok(dtoList);
        }
        /// <summary>
        /// Adds a franchise to a movie.
        /// </summary>
        /// <param name="franchiseId">The given ID of the franchise that is added to the movie.</param>
        /// <param name="movieId">The given ID of the movie that the franchise is added to.</param>
        /// <param name="franchiseMovieIds">DTO containing IDs of both Movie and Franchise</param>
        /// <returns>Created at action response with FranchiseMovieAddDTO object</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("/AddFranchiseToMovie/{franchiseId}/{movieId}")]
        public async Task<ActionResult> AddFranchiseToMovie([FromRoute] int franchiseId, [FromRoute] int movieId, [FromBody] FranchiseMovieAddDTO franchiseMovieIds)
        {
            if (franchiseId != franchiseMovieIds.FranchiseId || movieId != franchiseMovieIds.MovieId)
            {
                BadRequest();
            }
            Movie movie = _context.Movies.FirstOrDefault(c => c.Id == franchiseMovieIds.FranchiseId);
            if (movie == null)
            {
                return NotFound();
            }
            movie.FranchiseId = franchiseMovieIds.FranchiseId;
            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            return CreatedAtAction("AddFranchiseToMovie", franchiseMovieIds);
        }
        /// <summary>
        /// Removes a relationship between a franchise and a movie.
        /// </summary>
        /// <param name="franchiseId">The ID of the franchise that will lose the relation.</param>
        /// <param name="movieId">The ID of the movie that will lose the relation.</param>
        /// <param name="franchiseMovieIds"> The DTO containing the IDs of the franchise and the movie.</param>
        /// <returns>Http ok response</returns>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpDelete("/RemoveFranchiseFromMovie/{franchiseId}/{movieId}")]
        public async Task<ActionResult> RemoveFranchiseFromMovie([FromRoute] int franchiseId, [FromRoute] int movieId, [FromBody] FranchiseMovieAddDTO franchiseMovieIds)
        {
            if (franchiseId != franchiseMovieIds.FranchiseId || movieId != franchiseMovieIds.MovieId)
            {
                BadRequest();
            }
            Movie movie = _context.Movies.FirstOrDefault(c => c.Id == franchiseMovieIds.FranchiseId);
            if (movie == null)
            {
                return NotFound();
            }
            movie.FranchiseId = null;
            
            try
            {
                await _context.SaveChangesAsync();
            }

            catch
            {
                StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok();
        }
    }
}
